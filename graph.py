import numpy as np
import matplotlib.pyplot as plt


#plt.plotfile('test_file', delimiter=',', cols=(0, 1, 2), 
#             names=('col1', 'col2', 'col3'), marker='o')
#plt.show()

plt.plotfile('test_file', color='red', cols=(0, 1), delimiter=',')
plt.plotfile('test_file', color='blue', cols=(0, 2), newfig=False, delimiter=','),
#plt.plotfile('test_file', cols=(0, 3), delimiter=','),

plt.xlabel('x');
plt.ylabel('V,U');

plt.show()
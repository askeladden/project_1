#include <iostream>
#include <armadillo>
#include <fstream>
#include <iomanip> //setw


using namespace std;
using namespace arma;

ofstream ofile;

int main(int argc, char** argv)
  {
  long int i, j, N;
  double h, _h_squared_by_100; //step
  clock_t timer_start, timer_finish; //declare start/stop time

  cout << "Please enter matrix size: ";
  cin>>N;

  mat A(N, N);
  mat L(N, N);
  mat U(N, N);
  vec F(N);
  vec F_U(N);
  vec X(N);

  h=1.0/(N+1);
  _h_squared_by_100=h*h*100.0;

  A.fill(0.0);
  for (i = 0; i < N; i++) {
      X(i) = i*h;
      F(i)=_h_squared_by_100*exp(-10.0*(X(i)));
      F_U(i) = 1-(1-exp(-10.0))*X(i) - exp(-10.0*X(i));
      for (j = 0; j < N; j++) {
          if (i == j) A(i,j) = 2.0;
          if (abs(i - j) == 1) A(i,j) = -1.0;
      }
  }

  //cout << "det(A) = " << det(A) << endl;
  //A.print("A:");
  //X.print("X:");
  //F.print("F:");
  
  //A*v=f
  //L*U*v=f
  //L*y=f
  //y=U*V
  // LU decomposition start
  timer_start = clock();

  lu(L,U,A);
  vec y = solve(L,F);
  vec V = solve(U,y);

  timer_finish = clock();
  cout <<(timer_finish - timer_start)/(double)CLOCKS_PER_SEC << endl;

 // LU deconposition stop
 ofile.open("test_file", ios::app);
   for (i = 0; i < N; i++) {
      //ofile << setw(12) << setprecision(8) << X(i);
      //ofile << setw(12) << setprecision(8) << U(i);
      //ofile << setw(12) << setprecision(8) << V(i);
      //ofile << setw(12) << setprecision(8) << eps(i) << endl;
      ofile << setprecision(8) << X(i);
      ofile <<","<< setprecision(8) << F_U(i);
      ofile <<","<< setprecision(8) << V(i)<<endl;
  }  // end of function output

ofile.close();

  
  return 0;
  }
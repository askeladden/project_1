#include <iostream>
#include <armadillo>
#include <time.h>
#include <fstream>
#include <iomanip> //setw

using namespace std;
using namespace arma;

int main(int argc, char** argv)
  {

  long int i, N; //dummy index, matrix size
  ofstream ofile;
  double h, _h_squared_by_100; //step
  clock_t timer_start, timer_finish; //declare start/stop time

  cout << "Please enter matrix size: ";  //initialize matrix size
  cin>>N;

  //declare threediagonal matrix with three arrays
  rowvec E(N-1); //elements under/over main diagonal
  rowvec D(N); //main diagonal
  rowvec V(N); //unknovn vector

  //start of temporary vector declaration needed for forward subst
  rowvec D_temp(N);
  rowvec F_temp(N);
  //end of temporary vector declaration needed for forward subst

  rowvec X(N); //step values vector

  //analitycal solution
  rowvec F(N);
  rowvec U(N);

  //error vector
  rowvec eps(N);

  //fill in vectors and calculate temp constants
  D.fill(2.0);
  E.fill(-1.0);
  h=1.0/(N+1);
  _h_squared_by_100=h*h*100.0;

  // discretization of function F
  for (i=0; i <= N-1; i++) {
      X(i) = i*h;
      U(i) = 1-(1-exp(-10.0))*X(i) - exp(-10.0*X(i));
      F(i)=_h_squared_by_100*exp(-10*(X(i)));
  }

//FORWARD SUBSTITUTION
  timer_start = clock();

  F_temp(0)=F(0);
  D_temp(0)=D(0);

  for (i=1; i <= N-1; i++) {
      D_temp(i) = D(i) - (E(i-1)*E(i-1))/D_temp(i-1);
      F_temp(i) = F(i) - (E(i-1)*F_temp(i-1))/D_temp(i-1);
  }

//END OF FORWARD SUBSTITUTION

//BACKWARD SUBSTITUTION

  V(N-1)=F_temp(N-1)/D_temp(N-1);
  for (i=N-2; i>=1; i--) {
      V(i) = (F_temp(i) - E(i)*V(i+1))/D_temp(i);
  }

  timer_finish = clock();
//END OF BACKWARD SUBSTITUTION

//ERROR
  for(i=1; i<=N-1; i++) {
            eps(i) = log10(abs((V(i) - U(i))/U(i)));
  }

  //output
  ofile.open("test_file", ios::app);
  ofile << "RESULTS:" << endl;
  ofile << "Time of execution\t";
  ofile <<(timer_finish - timer_start)/(double)CLOCKS_PER_SEC << endl;
  for (i = 0; i < N-1; i++) {
      //ofile << setw(12) << setprecision(8) << X(i);
      //ofile << setw(12) << setprecision(8) << U(i);
      //ofile << setw(12) << setprecision(8) << V(i);
      //ofile << setw(12) << setprecision(8) << eps(i) << endl;
      ofile << setprecision(8) << X(i);
      ofile <<","<< setprecision(8) << U(i);
      ofile <<","<< setprecision(8) << V(i);
      ofile <<","<< setprecision(8) << eps(i) << endl;
  }  // end of function output

ofile.close();

  return 0;
  }